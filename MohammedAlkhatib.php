<?php

/**
 * Function to display a birthday message.
 *
 * @param $name
 * @return void
 */
function celebrateBirthday($name): void
{
    echo "🎉 Happy Birthday $name! 🎂 \n";
    echo "Wishing you a fantastic day filled with joy, success, and great accomplishments!\n";
    echo "Cheers to another year of inspiring leadership in the Development Unit!\n";
}

$unitHeadName = "Dr. Ubai Sandouk";

celebrateBirthday($unitHeadName);
