function createBirthdayMessage(name) {
  console.log(`%c
      🎉🎂🎈🎁🎊
  Happy Birthday, ${name}!
      🎉🎂🎈🎁🎊
  `, 'color: #00ccff; font-size: 24px;');
}

function createConfetti() {
  const confetti = ['🎉', '🎊', '🎈'];
  const randomConfetti = confetti[Math.floor(Math.random() * confetti.length)];
  const colors = ['#ffcc00', '#ff6699', '#00ccff', '#66ff33', '#9933ff'];
  const randomColor = colors[Math.floor(Math.random() * colors.length)];
  console.log(`%c${randomConfetti}`, `color: ${randomColor}; font-size: 24px;`);
}

function celebrateBirthday(name) {
  createBirthdayMessage(name);
  setInterval(createConfetti, 700);
}

celebrateBirthday('Dr. Ubai');