fun main() {
    val name = "Dr. Ubai"
    val birthdayMessage = generateBirthdayMessage(name)
    println(birthdayMessage)
}

fun generateBirthdayMessage(name: String): String {
    return """
        |Dear $name,
        |
        |Wishing you the happiest of birthdays! 🎉🎂 Another year has passed, and it's a joy to celebrate this special day with you.
        |May your day be filled with love, laughter, and all the things that bring you happiness.
        |
        |Cheers to another amazing year ahead! 🥳✨
        |
        |Warmest wishes,
        |Aghiad Odeh
    """.trimMargin()
}